'use strict';

let gulp = require('gulp');
let stylus = require('gulp-stylus');
let rename = require('gulp-rename');
let autoprefixer = require('autoprefixer-stylus');

gulp.task('build-stylesheets', function() {

  return gulp.src('./client/stylesheets/index.styl')
            .pipe(stylus({
              use: [autoprefixer()]
            }))
            .pipe(rename('bundle.css'))
            .pipe(gulp.dest('./server/static/styles'));

});

gulp.task('copy-assets', function() {
  return gulp.src('./client/assets/**/*.*', { base: './client/assets' })
             .pipe(gulp.dest('./server/static/assets'));
});

gulp.task('build-client', ['build-stylesheets', 'copy-assets']);
