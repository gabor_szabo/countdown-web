'use strict';

let webpack = require('webpack');

let production = process.env.NODE_ENV === 'production';

module.exports = {
  entry: './client/app/index.js',
  devtool: production ? 'source-map' : '',
  output: {
    path: __dirname + '/server/static/scripts',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.jade$/,
      loader: 'jade-loader'
    }]
  },
  plugins: production ? [
    new webpack.optimize.UglifyJsPlugin({ minimize: true })
  ] : []
};
