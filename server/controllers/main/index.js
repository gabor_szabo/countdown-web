'use strict';

let KoaRouter = require('koa-router');

let router = new KoaRouter();

router.get('/', require('./actions/index.get'));

module.exports = router;
