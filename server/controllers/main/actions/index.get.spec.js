'use strict';

let supertest = require('co-supertest');
let server = require('../../../');

describe('Countdown Web Main controller index action', function() {

  let response;

  beforeEach(function* () {
    response = yield supertest(server.listen()).get('/').end();
  });

  it('should return 200 status code', function() {
    this.expect(response.status).to.eql(200);
  });

  it('should respond with html content', function() {
    this.expect(response.type).to.eql('text/html');
  });

  it('should respond with the game tag in it', function() {
    this.expect(response.text).to.contain('<countdown-game>');
  });

});
