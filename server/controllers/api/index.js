'use strict';

let KoaRouter = require('koa-router');

let router = new KoaRouter();

router.get('/api/letters/solutions/:letters', require('./letters/actions/solutions.get'));

let numbersPuzzleActionHandlerFactory = require('./numbers/actions/puzzle');
let generateNumbersPuzzle = require('../../../numbers-round/generate-puzzle');
let InvalidPuzzleNumbersAmountError = require('../../../numbers-round/generate-puzzle/invalid-puzzle-numbers-amount-error');
let numbersPuzzleAction = numbersPuzzleActionHandlerFactory(generateNumbersPuzzle, [InvalidPuzzleNumbersAmountError]);
router.get('/api/numbers/puzzle', numbersPuzzleAction);

module.exports = router;
