'use strict';

const supertest = require('co-supertest');
const expect = require('chai').expect;
const koa = require('koa');
const createPuzzleActionHandler = require('./puzzle');

describe('Countdown Web GET api/numbers/puzzle', function() {

  const GENERATED_PUZZLE = { someKey: 'someValue' };

  it('returns a generated random puzzle', function* () {
    const puzzleGeneratorFn = () => GENERATED_PUZZLE;
    const testWebApp = koa();
    testWebApp.use(createPuzzleActionHandler(puzzleGeneratorFn, []));
    const response = yield supertest(testWebApp.listen()).get('/?big=3&little=3').end();
    expect(response.body).to.eql(GENERATED_PUZZLE);
  });

  it('passes the numeric value of big and little query parameters to the puzzle generator', function* () {
    const puzzleGeneratorFn = this.sinon.stub().returns(GENERATED_PUZZLE);
    const testWebApp = koa();
    testWebApp.use(createPuzzleActionHandler(puzzleGeneratorFn, []));
    yield supertest(testWebApp.listen()).get('/?big=2&little=4').end();
    expect(puzzleGeneratorFn).to.be.calledWith(2, 4);
  });

  describe('if the generator throws an expected error', function() {

    class ExpectedError1 extends Error { }
    class ExpectedError2 extends Error { }

    const testExpectedErrorResponse = function* testExpectedErrorResponse(error) {
      const puzzleGeneratorFn = () => { throw error; };
      const testWebApp = koa();
      testWebApp.use(createPuzzleActionHandler(puzzleGeneratorFn, [ExpectedError1, ExpectedError2]));
      const response = yield supertest(testWebApp.listen()).get('/?big=2&little=4').end();
      expect(response.status).to.eql(400);
    }

    it('returns a HTTP 400 response', function* () {
      yield testExpectedErrorResponse(new ExpectedError1());
      yield testExpectedErrorResponse(new ExpectedError2());
    });

  });

  describe('if the generator throws an unexpected error', function() {

    it('returns a HTTP 500 response', function* () {
      const puzzleGeneratorFn = this.sinon.stub().throws('TypeError');
      const testWebApp = koa();
      testWebApp.use(createPuzzleActionHandler(puzzleGeneratorFn, []));
      const response = yield supertest(testWebApp.listen()).get('/?big=2&little=4').end();
      expect(response.status).to.eql(500);
    });

  });

  describe('if the given amounts are not integer numbers', function() {

    const testHttp400Response = function* testHttp400Response(url) {
      const puzzleGeneratorFn = () => GENERATED_PUZZLE;
      const testWebApp = koa();
      testWebApp.use(createPuzzleActionHandler(puzzleGeneratorFn, []));
      const response = yield supertest(testWebApp.listen()).get(url).end();
      expect(response.status).to.eql(400);
    };

    describe('because they contain letters', function() {

      it('returns a HTTP 400 response', function* () {
        yield testHttp400Response('/?big=asdf&little=4');
        yield testHttp400Response('/?big=2&little=abc123');
        yield testHttp400Response('/?big=2asdf&little=4');
      });

    });

    describe('because they are non-integer numbers', function() {

      it('return a HTTP 400 response', function* () {
        yield testHttp400Response('/?big=2.5&little=3.5');
      });

    });

  });

});
