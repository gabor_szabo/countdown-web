'use strict';

class PuzzleAction {

  constructor(context, generatePuzzle, expectedErrors) {
    this._context = context;
    this._generatePuzzle = generatePuzzle;
    this._expectedErrors = expectedErrors;
  }

  *execute() {
    try {
      this._respondWithPuzzle();
    } catch (error) {
      if (this._isExpectedError(error)) {
        this._respondWithError(400);
      } else {
        this._respondWithError(500);
      }
    }
  }

  _respondWithPuzzle() {
    const bigCount = this._parseInteger(this._context.query.big);
    const littleCount = this._parseInteger(this._context.query.little);
    this._context.body = this._generatePuzzle(bigCount, littleCount);
  }

  _parseInteger(textValue) {
    const positiveIntegerPattern = /^([0-9]|[1-9][0-9]*)$/g;
    if (!positiveIntegerPattern.test(textValue)) {
      throw new InvalidNumberFormatError();
    }
    return parseInt(textValue);
  }

  _isExpectedError(error) {
    return  error instanceof InvalidNumberFormatError ||
            this._expectedErrors.some(errorType => error instanceof errorType);
  }

  _respondWithError(statusCode) {
    this._context.status = statusCode;
  }

}

class InvalidNumberFormatError extends Error { }

module.exports = function createPuzzleActionHandler(generatePuzzle, expectedErrors) {
  return function* puzzleActionHandler() {
    const puzzleAction = new PuzzleAction(this, generatePuzzle, expectedErrors);
    yield puzzleAction.execute();
  };
};
