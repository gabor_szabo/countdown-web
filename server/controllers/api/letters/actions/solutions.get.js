'use strict';

let httpRequest = require('../../../../lib/http-request');

module.exports = function* () {
  try {
    let letters = this.params.letters;
    let response = yield httpRequest.get(`http://anagramica.com/all/${letters}`);
    this.body = response.body;
  } catch (e) {
    this.status = 500;
  }
};
