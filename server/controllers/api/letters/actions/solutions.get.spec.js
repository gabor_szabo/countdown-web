'use strict';

let httpRequest = require('../../../../lib/http-request');
let supertest = require('co-supertest');
let server = require('../../../../index');

describe('Countdown web API controller letters solutions action', function() {

  let response;

  describe('when anagramica API responds well', function() {

    beforeEach(function* () {
      this.sandbox.stub(httpRequest, 'get').withArgs('http://anagramica.com/all/abcd').returns(Promise.resolve({ body: ANAGRAMICA_RESPONSE }));
      response = yield supertest(server.listen()).get('/api/letters/solutions/abcd').end();
    });

    it('should respond with 200 status code', function() {
      this.expect(response.statusCode).to.eql(200);
    });

    it('should respond json content type', function() {
      this.expect(response.type).to.eql('application/json');
    });

    it('should respond json content type', function() {
      this.expect(response.body).to.eql(ANAGRAMICA_RESPONSE);
    });

  });

  describe('when anagramica API is unavailable', function() {

    beforeEach(function* () {
      const error = Promise.reject({ error: 'some error message' });
      const supressWarnings = error.catch(() => {});
      this.sandbox.stub(httpRequest, 'get').withArgs('http://anagramica.com/all/abcd').returns(error);
      response = yield supertest(server.listen()).get('/api/letters/solutions/abcd').end();
    });

    it('should send a 500 status code', function() {
      this.expect(response.statusCode).to.eql(500);
    });

  });

});

const ANAGRAMICA_RESPONSE = {
  all: [
    'cab',
    'bad',
    'dab',
    'cad',
    'ad',
    'a',
    'b',
    'c',
    'd'
  ]
};
