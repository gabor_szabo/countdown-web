'use strict';

let fs = require('fs');
let path = require('path');

let folders = fs.readdirSync(__dirname).filter(entry => fs.statSync(path.join(__dirname, entry)).isDirectory());

module.exports = {

  use: function(koaApp) {
    folders.forEach(folder => {
      let router = require('./' + folder);
      koaApp.use(router.routes());
    });
  }

};
