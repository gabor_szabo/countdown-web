'use strict';

const PORT = process.env.PORT || 3000;

let koa = require('koa');
let views = require('koa-views');
let serve = require('koa-static');
let controllers = require('./controllers');

let app = koa();

app.use(views('views', { default: 'jade' }));
app.use(serve(__dirname + '/static'));
controllers.use(app);

if (!module.parent) {
  app.listen(PORT);
  console.log('Application started on port ' + PORT);
}

module.exports = app;
