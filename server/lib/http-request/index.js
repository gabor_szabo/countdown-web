'use strict';

let request = require('co-request');

module.exports = {
  'get': function(url) {
    return request.get(url);
  }
};
