FROM node:7.10.1-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY ./ /usr/src/app
RUN npm set unsafe-perm true
RUN npm install && npm cache clean --force
ENV NODE_ENV production
ENV PORT 80
EXPOSE 80
CMD [ "npm", "start" ]
