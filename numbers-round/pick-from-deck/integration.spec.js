'use strict';

const pickFromDeck = require('./');
const randomWrapper = require('../../util/random-wrapper');

class FakeRandomSequence {

  constructor(sequence) {
    this._sequence = sequence;
    this._nextIndex = 0;
    this.getNext = this.getNext.bind(this);
  }

  getNext() {
    return this._sequence[this._nextIndex++];
  }

}

describe('Pick From Deck (integration)', function() {

  afterEach(function() {
    randomWrapper.restore();
  });

  it('should be wired correctly', function() {
    const randomFunction = new FakeRandomSequence([0.6, 0.7, 0.15, 0.1, 0.01, 0.75]).getNext;
    randomWrapper.stub(randomFunction);
    const result = pickFromDeck([25, 50, 75, 100], 3);
    this.expect(result).to.eql([75, 25, 100]);
  });

});
