'use strict';

module.exports = function pickFromDeck(getUniqueIndexes, deck, amountToPick) {
  const indexes = getUniqueIndexes(deck.length, amountToPick);
  return indexes.map(index => deck[index]);
};
