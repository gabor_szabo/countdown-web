'use strict';

const pickFromDeck = require('./pick-from-deck');
const expect = require('chai').expect;

const createUniqueIndexChooser = function(expectedLimit, expectedN, result) {
  return function getUniqueIndexes(limit, n) {
    expect(limit).to.eql(expectedLimit);
    expect(n).to.eql(expectedN);
    return result;
  };
};

describe('Pick from deck function', function() {

  it('should return an empty list for picking zero elements', function() {
    const uniqueIndexChooser = createUniqueIndexChooser(3, 0, []);
    const deck = [1, 2, 3];
    const amountToPick = 0;
    const result = pickFromDeck(uniqueIndexChooser, deck, amountToPick);
    this.expect(result).to.eql([]);
  });

  it('picking 1 element', function() {
    const uniqueIndexChooser = createUniqueIndexChooser(3, 1, [2]);
    const result = pickFromDeck(uniqueIndexChooser, [1, 2, 3], 1);
    this.expect(result).to.eql([3]);
  });

  it('picking 1 element (version 2)', function() {
    const uniqueIndexChooser = createUniqueIndexChooser(3, 1, [0]);
    const result = pickFromDeck(uniqueIndexChooser, [1, 2, 3], 1);
    this.expect(result).to.eql([1]);
  });

  it('picking 2 elements', function() {
    const uniqueIndexChooser = createUniqueIndexChooser(5, 2, [3, 0]);
    const result = pickFromDeck(uniqueIndexChooser, [1, 2, 3, 4, 5], 2);
    this.expect(result).to.eql([4, 1]);
  });

});
