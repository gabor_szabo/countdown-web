'use strict';

const _ = require('lodash');
const pickFromDeckWithDeps = require('./pick-from-deck');

module.exports = _.curry(pickFromDeckWithDeps)(require('../../util/get-unique-indexes'));
