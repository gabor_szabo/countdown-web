'use strict';

const pickFromDeck = require('../pick-from-deck');

const BIG_NUMBERS = [25, 50, 75, 100];
const LITTLE_NUMBERS = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10];

module.exports = function getPuzzleNumbers(littleAmount, bigAmount) {
  return [...pickFromDeck(LITTLE_NUMBERS, littleAmount), ...pickFromDeck(BIG_NUMBERS, bigAmount)];
};
