'use strict';

const getPuzzleNumbers = require('./');
const randomWrapper = require('../../util/random-wrapper');

class FakeRandomSequence {

  constructor(sequence) {
    this._sequence = sequence;
    this._nextIndex = 0;
    this.getNext = this.getNext.bind(this);
  }

  getNext() {
    return this._sequence[this._nextIndex++];
  }

}

describe('Puzzle Numbers Creator', function() {

  afterEach(function() {
    randomWrapper.restore();
  });

  it('should return an empty list when asking for zero numbers', function() {
    const result = getPuzzleNumbers(0, 0);
    this.expect(result).to.eql([]);
  });

  [
    { randomValue: 0.1, expectedResult: 25 },
    { randomValue: 0.3, expectedResult: 50 },
    { randomValue: 0.55, expectedResult: 75 },
    { randomValue: 0.75, expectedResult: 100 }
  ].forEach(testCase => {

    it(`should pick one from the big numbers deck when asking for 0 little, 1 big numbers (${testCase.expectedResult})`, function() {
      const randomFn = new FakeRandomSequence([testCase.randomValue]).getNext;
      randomWrapper.stub(randomFn);
      const result = getPuzzleNumbers(0, 1);
      this.expect(result).to.eql([testCase.expectedResult]);
    });

  });

  [
    { randomValue: 0.03, expectedResult: 1 }, { randomValue: 0.08, expectedResult: 1 },
    { randomValue: 0.13, expectedResult: 2 }, { randomValue: 0.18, expectedResult: 2 },
    { randomValue: 0.23, expectedResult: 3 }, { randomValue: 0.28, expectedResult: 3 },
    { randomValue: 0.33, expectedResult: 4 }, { randomValue: 0.38, expectedResult: 4 },
    { randomValue: 0.43, expectedResult: 5 }, { randomValue: 0.48, expectedResult: 5 },
    { randomValue: 0.53, expectedResult: 6 }, { randomValue: 0.58, expectedResult: 6 },
    { randomValue: 0.63, expectedResult: 7 }, { randomValue: 0.68, expectedResult: 7 },
    { randomValue: 0.73, expectedResult: 8 }, { randomValue: 0.78, expectedResult: 8 },
    { randomValue: 0.83, expectedResult: 9 }, { randomValue: 0.88, expectedResult: 9 },
    { randomValue: 0.93, expectedResult: 10 }, { randomValue: 0.98, expectedResult: 10 },
  ].forEach(testCase => {

    it(`should pick one from the little numbers deck when asking for 1 little, 0 big numbers (${testCase.expectedResult})`, function() {
      const randomFn = new FakeRandomSequence([testCase.randomValue]).getNext;
      randomWrapper.stub(randomFn);
      const result = getPuzzleNumbers(1, 0);
      this.expect(result).to.eql([testCase.expectedResult]);
    });

  });

  it('should pick two from the little numbers when asking for 2 little, 0 big numbers', function() {
    const randomFn = new FakeRandomSequence([0.13, 0.11, 0.12, 0.53]).getNext;
    randomWrapper.stub(randomFn);
    const result = getPuzzleNumbers(2, 0);
    this.expect(result).to.eql([2, 6]);
  });

  it('should pick two from the big numbers when asking for 0 little, 2 big numbers', function() {
    const randomFn = new FakeRandomSequence([0.48, 0.35, 0.4, 0.19]).getNext;
    randomWrapper.stub(randomFn);
    const result = getPuzzleNumbers(0, 2);
    this.expect(result).to.eql([50, 25]);
  });

  it('little numbers should come before big numbers', function() {
    const randomFn = new FakeRandomSequence([0.78, 0.76, 0.13, 0.3, 0.4, 0.5, 0.9]).getNext;
    randomWrapper.stub(randomFn);
    const result = getPuzzleNumbers(2, 3);
    this.expect(result).to.eql([8, 2, 50, 75, 100]);
  });


});
