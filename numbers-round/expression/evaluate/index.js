'use strict';

const evaluateTree = function evaluateTree(root) {
  if (!root) return 0;
  if (root.type === 'singlevalue') return root.value;
  return evaluateFlat(evaluateTree(root.lhs), root.operator, evaluateTree(root.rhs));
};

const evaluateFlat = function evaluateFlat(lhsValue, operator, rhsValue) {
  switch (operator) {
    case '+':
      return lhsValue + rhsValue;
    case '-':
      return lhsValue - rhsValue;
    case '*':
      return lhsValue * rhsValue;
    case '/':
      return lhsValue / rhsValue;
  }
};

module.exports = evaluateTree;
