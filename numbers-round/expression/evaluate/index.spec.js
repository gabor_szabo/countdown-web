'use strict';

const expect = require('chai').expect;
const { singleValue, binary } = require('../factory');

const evaluateTree = require('./');

describe('Expression tree evaluator', function() {

  it('should return 0 for null', function() {
    const result = evaluateTree(null);
    expect(result).to.eql(0);
  });

  describe('if a single value is given', function() {

    it('should return the value of the given value', function() {
      const expressionTree = singleValue(5);
      const result = evaluateTree(expressionTree);
      expect(result).to.eql(5);
    });

  });

  [
    { operator: '+', expectedResult: 1+2, name: 'sum' },
    { operator: '-', expectedResult: 1-2, name: 'difference' },
    { operator: '*', expectedResult: 1*2, name: 'multiplication' },
    { operator: '/', expectedResult: 1/2, name: 'division' }
  ].forEach(testCase => {

    describe(`if a ${testCase.name} operation is given with two single values`, function() {

      it(`should return the ${testCase.name} of the two values`, function() {
        const expressionTree = binary(
          singleValue(1),
          testCase.operator,
          singleValue(2)
        );
        const result = evaluateTree(expressionTree);
        expect(result).to.eql(testCase.expectedResult);
      });

    });

  });

  describe('if the left side of the binary operation is a binary operation', function() {

    it('should evaluate the left side', function() {
      const expressionTree = binary(
        binary(singleValue(3), '+', singleValue(5)),
        '/',
        singleValue(7)
      );
      const result = evaluateTree(expressionTree);
      expect(result).to.eql((3 + 5) / 7);
    });

  });

  describe('if the right side of the binary operation is a binary operation', function() {

    it('should evaluate the right side', function() {
      const expressionTree = binary(
        singleValue(7),
        '/',
        binary(singleValue(3), '-', singleValue(5))
      );
      const result = evaluateTree(expressionTree);
      expect(result).to.eql(7 / (3 - 5));
    });

  });

  it('should evaluate multi-level trees', function() {
    const expressionTree = binary(
      binary(
        binary(singleValue(3), '-', singleValue(5)),
        '+',
        singleValue(7)
      ),
      '/',
      binary(
        binary(singleValue(11), '*', singleValue(13)),
        '/',
        binary(singleValue(17), '+', singleValue(19))
      )
    );
    const result = evaluateTree(expressionTree);
    expect(result).to.eql(((3 - 5) + 7) / ((11 * 13) / (17 + 19)));
  });

});
