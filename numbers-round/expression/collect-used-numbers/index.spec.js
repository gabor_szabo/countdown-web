'use strict';

const expect = require('chai').expect;
const collectUsedNumbers = require('./');

const { singleValue, binary } = require('../factory');

describe('Expression tree used numbers collector', function() {

  describe('when given a single value', function() {

    it('should return a one-element list containing the given value (5)', function() {
      const expressionTree = singleValue(5);
      const usedNumbers = collectUsedNumbers(expressionTree);
      expect(usedNumbers).to.eql([5]);
    });

    it('should return a one-element list containing the given value (3)', function() {
      const expressionTree = singleValue(3);
      const usedNumbers = collectUsedNumbers(expressionTree);
      expect(usedNumbers).to.eql([3]);
    });

  });

  describe('when a binary operation of two single value is given', function() {

    describe('and the two operands are single values', function() {

      it('should return the two single values in a list', function() {
        const expressionTree = binary(singleValue(8), '/', singleValue(3));
        const usedNumbers = collectUsedNumbers(expressionTree);
        expect(usedNumbers).to.eql([8, 3]);
      });

    });

    describe('and the left-hand side is a binary operation', function() {

      it('should include all used values on the left hand side', function() {
        const expressionTree = binary(
          binary(singleValue(8), '-', singleValue(9)),
          '/',
          singleValue(3)
        );
        const usedNumbers = collectUsedNumbers(expressionTree);
        expect(usedNumbers).to.eql([8, 9, 3]);
      });

    });

    describe('and the right-hand side is a binary operation', function() {

      it('should include all used values on the right hand side', function() {
        const expressionTree = binary(
          singleValue(10),
          '*',
          binary(singleValue(9), '/', singleValue(123))
        );
        const usedNumbers = collectUsedNumbers(expressionTree);
        expect(usedNumbers).to.eql([10, 9, 123]);
      });

    });

  });

  it('should return all the used values in a deeply nested expression tree', function() {
    const expressionTree = binary(
      binary(
        binary(singleValue(3), '-', singleValue(5)),
        '+',
        singleValue(7)
      ),
      '/',
      binary(
        binary(singleValue(11), '*', singleValue(13)),
        '/',
        binary(singleValue(17), '+', singleValue(19))
      )
    );
    const usedNumbers = collectUsedNumbers(expressionTree);
    expect(usedNumbers).to.eql([3, 5, 7, 11, 13, 17, 19]);
  });

});
