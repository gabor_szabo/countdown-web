'use strict';

const collectUsedNumbers = function collectUsedNumbers(root) {
  if (root.type === 'singlevalue') return [root.value];
  return [...collectUsedNumbers(root.lhs), ...collectUsedNumbers(root.rhs)];
};

module.exports = collectUsedNumbers;
