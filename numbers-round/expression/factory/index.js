'use strict';

function singleValue(value) {
  return { type: 'singlevalue', value: value };
}

function binary(lhs, operator, rhs) {
  if (typeof lhs === 'number') lhs = singleValue(lhs);
  if (typeof rhs === 'number') rhs = singleValue(rhs);
  return { type: 'binaryoperator', operator: operator, lhs: lhs, rhs: rhs };
}

module.exports = {
  singleValue,
  binary
}
