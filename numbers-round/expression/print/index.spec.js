'use strict';

const printTree = require('./');
const { singleValue, binary } = require('../factory');
const expect = require('chai').expect;

describe('Expression tree printer', function() {

  const test = function test(expression, expectedText) {
    const expressionText = printTree(expression);
    expect(expressionText).to.eql(expectedText);
  }

  it('should print empty string for null', function() {
    test(null, '');
  });

  it('should print single values as is', function() {
    test(singleValue(5), '5');
    test(singleValue(7), '7');
  });

  it('should print a binary expression of two single values without parens', function() {
    test(binary(1, '+', 2), '1 + 2');
    test(binary(1, '-', 2), '1 - 2');
    test(binary(1, '/', 3), '1 / 3');
    test(binary(8, '*', 3), '8 * 3');
  });

  it('should print a three element sum without parens', function() {
    const leftSingleValueVersion = binary(1, '+', binary(2, '+', 3));
    test(leftSingleValueVersion, '1 + 2 + 3');
    const rightSingleValueVersion = binary(binary(2, '+', 3), '+', 1);
    test(rightSingleValueVersion, '2 + 3 + 1');
  });

  it('should print a three element product without parens', function() {
    test(binary(1, '*', binary(2, '*', 3)), '1 * 2 * 3');
    test(binary(binary(1, '*', 2), '*', 3), '1 * 2 * 3');
  });

  it('should add parens when subtracting a sum', function() {
    const subtractingSum = binary(1, '-', binary(2, '+', 3));
    test(subtractingSum, '1 - (2 + 3)');
  });

  it('should add parens when subtracting a difference', function() {
    const subtractingSum = binary(1, '-', binary(2, '-', 3));
    test(subtractingSum, '1 - (2 - 3)');
  });

  it('should not add parens when subtracting a product or divison', function() {
    test(binary(1, '-', binary(2, '*', 3)), '1 - 2 * 3');
    test(binary(1, '-', binary(2, '/', 3)), '1 - 2 / 3');
  });

  it('should add parens when dividing with a binary expression', function() {
    test(binary(1, '/', binary(2, '*', 3)), '1 / (2 * 3)');
    test(binary(1, '/', binary(2, '+', 3)), '1 / (2 + 3)');
    test(binary(1, '/', binary(2, '-', 3)), '1 / (2 - 3)');
  });

  it('should add parens when multiplying with a sum', function() {
    test(binary(1, '*', binary(2, '+', 3)), '1 * (2 + 3)');
  });

  it('should add parens when multiplying with a difference', function() {
    test(binary(1, '*', binary(2, '-', 3)), '1 * (2 - 3)');
  });

  it('should not add parens when multiplying with a division', function() {
    test(binary(1, '*', binary(2, '/', 3)), '1 * 2 / 3');
  });

  it('should add parens when multiplying a sum', function() {
    test(binary(binary(1, '+', 2), '*', 3), '(1 + 2) * 3');
  });

  it('should add parens when multiplying a difference', function() {
    test(binary(binary(1, '-', 2), '*', 3), '(1 - 2) * 3');
  });

  it('should not add parens when multiplying a division', function() {
    test(binary(binary(1, '/', 2), '*', 3), '1 / 2 * 3');
  });

  it('should add parens when dividing a sum', function() {
    test(binary(binary(1, '+', 2), '/', 3), '(1 + 2) / 3');
  });

  it('should add parens when dividing a difference', function() {
    test(binary(binary(1, '-', 2), '/', 3), '(1 - 2) / 3');
  });

  it('should not add parens when dividing a product', function() {
    test(binary(binary(1, '*', 2), '/', 3), '1 * 2 / 3');
  });

  it('should not add parens when dividing a division', function() {
    test(binary(binary(1, '/', 2), '/', 3), '1 / 2 / 3');
  });

  it('should work correctly with a complicated example', function() {
    const expression = binary(
      binary(
        1,
        '-',
        binary(
          binary(2, '-', 3),
          '*',
          4
        )
      ),
      '/',
      binary(
        binary(
          5,
          '-',
          binary(6, '-', 7)
        ),
        '+',
        binary(8, '*', 9)
      )
    );
    test(expression, '(1 - (2 - 3) * 4) / (5 - (6 - 7) + 8 * 9)');
  });

});
