'use strict';

function printTree(root) {
  if (!root) return '';
  if (root.type === 'singlevalue') return root.value.toString();
  return `${printLhs(root.lhs, root.operator)} ${root.operator} ${printRhs(root.rhs, root.operator)}`;
};

function printLhs(lhs, parentOperator) {
  if (lhs.type === 'singlevalue') return printTree(lhs);
  if (parentOperator === '*' && (lhs.operator === '+' || lhs.operator === '-')) {
    return `(${printTree(lhs)})`;
  }
  if (parentOperator === '/' && (lhs.operator === '+' || lhs.operator === '-')) {
    return `(${printTree(lhs)})`;
  }
  return printTree(lhs);
}

function printRhs(rhs, parentOperator) {
  if (rhs.type === 'singlevalue') return printTree(rhs);
  if (parentOperator === '-' && (rhs.operator === '+' || rhs.operator === '-')) {
    return `(${printTree(rhs)})`;
  }
  if (parentOperator === '/') {
    return `(${printTree(rhs)})`;
  }
  if (parentOperator === '*' && (rhs.operator === '+' || rhs.operator === '-')) {
    return `(${printTree(rhs)})`;
  }
  return printTree(rhs);
}

module.exports = printTree;
