'use strict';

const hashSetFactory = require('hash-set');
const hashFn = combination => (combination.usedBits + '|' + combination.value);
const HashSet = hashSetFactory(hashFn);

module.exports = HashSet;
