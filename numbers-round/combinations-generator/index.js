'use strict';

const { singleValue } = require('../expression/factory');
const _ = require('lodash');
const HashSet = require('./hash-set');
const CombinationsForExpressionSizeGenerator = require('./combinations-for-expression-size-generator');

class CombinationsGenerator {

  constructor(numbers) {
    this._numbers = numbers;
    this._combinationsByExpressionSize = [new HashSet()];
  }

  generate() {
    this._generateCombinationsByExpressionSize();
    return this._mergeCombinations();
  }

  _generateCombinationsByExpressionSize() {
    this._generateOneSizeExpressionCombinations();
    for (let expressionSize = 2; expressionSize <= this._numbers.length; expressionSize++) {
      this._generateCombinationsForSize(expressionSize);
    }
  }

  _mergeCombinations() {
    return _(this._combinationsByExpressionSize)
            .map(combinations => Array.from(combinations))
            .flatten()
            .map(c => ({ value: c.value, expression: c.expression }))
            .value();
  }

  _generateOneSizeExpressionCombinations() {
    const oneSizeExpressionCombinations = this._numbers.map((number, index) => ({
      value: number,
      expression: singleValue(number),
      usedBits: 1 << index
    }));
    this._combinationsByExpressionSize[1] = new HashSet(oneSizeExpressionCombinations);
  }

  _generateCombinationsForSize(expressionSize) {
    const generatorForSize
      = new CombinationsForExpressionSizeGenerator(expressionSize, this._combinationsByExpressionSize);
    this._combinationsByExpressionSize[expressionSize] = generatorForSize.generate();
  }

}

module.exports = function(numbers) {
  const generator = new CombinationsGenerator(numbers);
  return generator.generate();
};
