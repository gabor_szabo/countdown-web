'use strict';

const evaluateTree = require('../expression/evaluate');
const printTree = require('../expression/print');
const usedNumbers = require('../expression/collect-used-numbers');
const _ = require('lodash');

module.exports = function(chai, utils) {
  const Assertion = chai.Assertion;
  Assertion.addMethod('includeEquivalent', includeEquivalent);
};

function includeEquivalent(sampleCombination) {
  const combinations = this._obj;
  const foundMatchingCombination = hasMatchingCombination(combinations, sampleCombination);
  this.assert(
    foundMatchingCombination,
    `expected to find ${printTree(sampleCombination.expression)}`,
    `expected not to find ${printTree(sampleCombination.expression)}`
  );
}

function hasMatchingCombination(combinations, sampleCombination) {
  return _(combinations)
      .filter(combination => haveSameValue(combination, sampleCombination))
      .filter(combination => useSameNumbers(combination, sampleCombination))
      .some();
}

function haveSameValue(oneCombination, otherCombination) {
  return Math.abs(oneCombination.value - otherCombination.value) < 0.001 &&
    evaluateTree(oneCombination.expression) === evaluateTree(otherCombination.expression);
}

function useSameNumbers(oneCombination, otherCombination) {
  const oneUsedNumbers = usedNumbers(oneCombination.expression);
  const otherUsedNumbers = usedNumbers(otherCombination.expression);
  return oneUsedNumbers.length === otherUsedNumbers.length &&
    _.difference(oneUsedNumbers, otherUsedNumbers).length === 0;
}
