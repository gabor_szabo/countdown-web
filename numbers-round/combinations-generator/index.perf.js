'use strict';

const generateCombinations = require('./');
const chai = require('chai');
const { expect } = chai;
const { singleValue, binary } = require('../expression/factory');
const includeEquivalent = require('./include-equivalent');

chai.use(includeEquivalent);

describe('Combinations generator performance test', function() {

  const testIncludes = function testNumbers(numbers, expectedToInclude) {
    const combinations = generateCombinations(numbers);
    expect(combinations).to.includeEquivalent(expectedToInclude);
  };

  it('should generate combinations for 6 numbers in 5 secs', function() {
    this.timeout(5000);
    testIncludes([3, 7, 11, 13, 17, 19], { value: 7+3, expression: binary(7, '+', 3) });
  });

});
