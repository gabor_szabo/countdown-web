'use strict';

const generateCombinations = require('./');
const chai = require('chai');
const { expect } = chai;
const { singleValue, binary } = require('../expression/factory');
const includeEquivalent = require('./include-equivalent');

chai.use(includeEquivalent);

describe('Combinations generator', function() {

  describe('the generated combination of the input numbers', function() {

    describe('when zero numbers are given', function() {

      it('should be an empty list', function() {
        const numbers = [];
        const combinations = generateCombinations(numbers);
        expect(combinations).to.eql([]);
      });

    });

    describe('when one number is given', function() {

      const testOneNumber = function testOneNumber(testNumber) {
        const numbers = [testNumber];
        const combinations = generateCombinations(numbers);
        expect(combinations).to.eql([{
          value: testNumber,
          expression: singleValue(testNumber)
        }]);
      };

      it('should be the given number as a single value', function() {
        testOneNumber(1);
        testOneNumber(2);
      });

    });

    const testIncludes = function testNumbers(numbers, expectedToInclude) {
      const combinations = generateCombinations(numbers);
      expect(combinations).to.includeEquivalent(expectedToInclude);
    };

    const testDoesNotInclude = function testNumbers(numbers, expectedToInclude) {
      const combinations = generateCombinations(numbers);
      expect(combinations).not.to.includeEquivalent(expectedToInclude);
    };

    describe('when a1 and a2 are given', function() {

      it('should include a2', function() {
        const numbers = [3, 7];
        const combinations = generateCombinations(numbers);
        expect(combinations).to.includeEquivalent({ value: 7, expression: singleValue(7) });
      });

      it('should include a2+a1', function() {
        testIncludes([3, 7], { value: 7+3, expression: binary(7, '+', 3) });
        testIncludes([15, 5], { value: 15+5, expression: binary(15, '+', 5) });
      });

      it('should include a1-a2', function() {
        testIncludes([3, 7], { value: 3-7, expression: binary(3, '-', 7) });
      });

      it('should include a2-a1', function() {
        testIncludes([3, 7], { value: 7-3, expression: binary(7, '-', 3) });
      });

      it('should include a1*a2', function() {
        testIncludes([3, 7], { value: 3*7, expression: binary(3, '*', 7) });
      });

      it('should include a1/a2 if a2 is not zero', function() {
        testIncludes([3, 7], { value: 3/7, expression: binary(3, '/', 7) });
      });

      it('should not include a1/a2 if a2 is zero', function() {
        testDoesNotInclude([3, 0], { value: 3/0, expression: binary(3, '/', 0) });
      });

      it('should include a2/a1 if a1 is not zero', function() {
        testIncludes([3, 7], { value: 7/3, expression: binary(7, '/', 3) });
      });

      it('should not include a2/a1 if a1 is zero', function() {
        testDoesNotInclude([0, 7], { value: 7/0, expression: binary(7, '/', 0) });
      });

    });

    describe('when a1, a2 and a3 are given', function() {

      it('should include a2*a3', function() {
        testIncludes([3, 7, 11], { value: 7*11, expression: binary(7, '*', 11) });
      });

      it('should include a1/a3', function() {
        testIncludes([3, 7, 11], { value: 3/11, expression: binary(3, '/', 11) });
      });

      it('should include a1 + a2*a3', function() {
        testIncludes([3, 7, 11], { value: 3 + 7*11, expression: binary(3, '+', binary(7, '*', 11)) });
      });

      it('should include a3*a1 - a2', function() {
        testIncludes([3, 7, 11], { value: 11/3 - 7, expression: binary(binary(11, '/', 3), '-', 7) });
      });

      it('should not include a1 + a1 + a2', function() {
        testDoesNotInclude([3, 7, 11], { value: 3 + 3 + 7, expression: binary(3, '+', binary(3, '+', 7)) });
      });

    });

    describe('when a1, a2, a3 and a4 are given', function() {

      it('should include a4-a1', function() {
        testIncludes([3, 7, 11, 17], { value: 17-3, expression: binary(17, '-', 3) });
      });

      it('should include a4 * a3 / a1', function() {
        testIncludes([3, 7, 11, 17], { value: 17 * 11 / 3, expression: binary(17, '*', binary(11, '/', 3)) });
      });

      it('should include a2 - a3 / a4 * a1 (1 term + 3 term)', function() {
        testIncludes([3, 7, 11, 17], {
          value: 7 - 11 / 17 * 3,
          expression: binary( 7, '-', binary(binary(11, '/', 17), '*', 3) )
        });
      });

      it('should include (a1 + a3) / (a4 - a2) (2 term + 2 term)', function() {
        testIncludes([3, 7, 11, 17], {
          value: (3 + 11) / (17 - 7),
          expression: binary( binary(3, '+', 11), '/', binary(17, '-', 7) )
        });
      });

      it('should not include a1 + a1*a2*a4', function() {
        testDoesNotInclude([3, 7, 11, 17], {
          value: 3 + 3*7*17,
          expression: binary(3, '+', binary(3, '*', binary(7, '*', 17)))
        });
      });

      it('should not include (a1 + a2) * (a2 + a4)', function() {
        testDoesNotInclude([3, 7, 11, 17], {
          value: (3 + 7) * (7 + 17),
          expression: binary(binary(3, '+', 7), '*', binary(7, '+', 17))
        });
      });

    });

    describe('when a1 and a1 are given (two same numbers)', function() {

      it('should include a1 + a1', function() {
        testIncludes([3, 3], { value: 3+3, expression: binary(3, '+', 3) });
      });

    });

    describe('when 5 numbers are given (integration test)', function() {

      it('should include (a2 - a5*a3) / (a1 + a4)', function() {
        this.timeout(20000);
        testIncludes([3, 3, 7, 11, 17], {
          value: (3 - 17 * 7) / (3 + 11),
          expression: binary(binary(3, '-', binary(17, '*', 7)), '/', binary(3, '+', 11))
        });
      });

    });

  });

});
