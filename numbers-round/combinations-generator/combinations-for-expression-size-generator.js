'use strict';

const { binary } = require('../expression/factory');
const HashSet = require('./hash-set');

const ADD = (a, b) => a+b;
const SUBTRACT = (a, b) => a-b;
const TIMES = (a, b) => a*b;
const DIV = (a, b) => a/b;

class CombinationsForExpressionSizeGenerator {

  constructor(expressionSize, previousCombinations) {
    this._expressionSize = expressionSize;
    this._previousCombinations = previousCombinations;
    this._result = new HashSet();
  }

  generate() {
    for (let subExpressionSize = 1; subExpressionSize <= this._expressionSize / 2; subExpressionSize++) {
      this._combineTwoSets(
        this._previousCombinations[subExpressionSize],
        this._previousCombinations[this._expressionSize - subExpressionSize]
      );
    }
    return this._result;
  }

  _combineTwoSets(oneSet, otherSet) {
    for (let oneCombination of oneSet) {
      for (let otherCombination of otherSet) {
        if (this._isPairCombinable(oneCombination, otherCombination)) {
          this._combineTwoCombinations(oneCombination, otherCombination)
        }
      }
    }
  }

  _isPairCombinable(oneCombination, otherCombination) {
    return (oneCombination.usedBits & otherCombination.usedBits) === 0;
  }

  _combineTwoCombinations(oneCombination, otherCombination) {
    this._addToResult(oneCombination, otherCombination, ADD, '+');
    this._addToResult(oneCombination, otherCombination, SUBTRACT, '-');
    this._addToResult(otherCombination, oneCombination, SUBTRACT, '-');
    this._addToResult(oneCombination, otherCombination, TIMES, '*');
    if (otherCombination.value !== 0) this._addToResult(oneCombination, otherCombination, DIV, '/');
    if (oneCombination.value !== 0) this._addToResult(otherCombination, oneCombination, DIV, '/');
  }

  _addToResult(oneCombination, otherCombination, operation, operator) {
    this._result.add({
      value: operation(oneCombination.value, otherCombination.value),
      expression: binary(oneCombination.expression, operator, otherCombination.expression),
      usedBits: oneCombination.usedBits | otherCombination.usedBits
    });
  }

}

module.exports = CombinationsForExpressionSizeGenerator;
