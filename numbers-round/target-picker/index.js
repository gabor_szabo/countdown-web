'use strict';

const pickFromDeck = require('../pick-from-deck');
const _ = require('lodash');

module.exports = function pickTarget(combinations) {
  const valuesToPickFrom = _(combinations)
                            .map(c => c.value)
                            .filter(value => 100 < value && value < 1000 && Number.isInteger(value))
                            .uniq()
                            .value();
  if (valuesToPickFrom.length === 0) return 0;
  const pickedValueAsList = pickFromDeck(valuesToPickFrom, 1);
  return pickedValueAsList[0];
};
