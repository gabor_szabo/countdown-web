'use strict';

const pickTarget = require('./');
const { expect } = require('chai');
const randomWrapper = require('../../util/random-wrapper');

describe('Numbers round target number picker', function() {

  afterEach(function() {
    randomWrapper.restore();
  });

  const testTarget = function testTarget(combinations, expectedTarget, randomValue) {
    if (randomValue !== undefined) randomWrapper.stub(() => randomValue);
    const target = pickTarget(combinations);
    expect(target).to.eql(expectedTarget);
  };

  describe('when the combination list is empty', function() {

    it('should pick 0', function() {
      testTarget([], 0);
    });

  });

  describe('when the combination list has one element', function() {

    it('should pick the value of the combination', function() {
      testTarget([{ value: 101 }], 101);
    });

    it('should pick 0 if the value is not in [101; 999]', function() {
      testTarget([{ value: 1 }], 0);
    });

  });

  describe('when the combination list has more elements', function() {

    it('should pick a random value of the combinations', function() {
      const combinations = [{ value: 101 }, { value: 102 }];
      testTarget(combinations, 101, 0.4);
      testTarget(combinations, 102, 0.6);
    });

    it('should only pick from integer values', function() {
      const combinations = [{ value: 101 }, { value: 101.5 }, { value: 102 }];
      testTarget(combinations, 102, 0.6);
    });

    it('should only pick from the [101; 999] interval', function() {
      const combinations = [{ value: 1 }, { value: 100 }, { value: 1000 }, { value: 212 }];
      testTarget(combinations, 212, 0.1);
    });

    it('should pick evenly among values, even if a value occurs multiple times', function() {
      const combinations = [{ value: 101 }, { value: 101 }, { value: 102 }];
      testTarget(combinations, 102, 0.6);
    });

  });

});
