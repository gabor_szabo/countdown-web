'use strict';

const _ = require('lodash');
const generatePuzzleWithDeps = require('./puzzle-generator');

module.exports = _.curry(generatePuzzleWithDeps)({
  puzzleNumbersGenerator: require('../puzzle-numbers-creator'),
  combinationsGenerator: require('../combinations-generator'),
  targetPicker: require('../target-picker')
});
