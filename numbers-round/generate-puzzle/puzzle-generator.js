'use strict';

const InvalidPuzzleNumbersAmountError = require('./invalid-puzzle-numbers-amount-error');
const printExpression = require('../expression/print');

class PuzzleGenerator {

  constructor(puzzleNumbersGenerator, combinationsGenerator, targetPicker, bigCount, littleCount) {
    this._puzzleNumbersGenerator = puzzleNumbersGenerator;
    this._combinationsGenerator = combinationsGenerator;
    this._targetPicker = targetPicker;
    this._bigCount = bigCount;
    this._littleCount = littleCount;
  }

  generate() {
    this._validateAmounts();
    const numbers = this._puzzleNumbersGenerator(this._littleCount, this._bigCount);
    const { target, solutions } = this._pickTargetWithSolutions(numbers);
    return { numbers, target, solutions };
  }

  _validateAmounts() {
    if (!this._areValidAmounts())
      throw new InvalidPuzzleNumbersAmountError();
  }

  _areValidAmounts() {
    return  0 <= this._bigCount && this._bigCount <= 4 &&
            0 <= this._littleCount && this._littleCount <= 6 &&
            this._bigCount + this._littleCount === 6;
  }

  _pickTargetWithSolutions(numbers) {
    const combinations = this._combinationsGenerator(numbers);
    const target = this._targetPicker(combinations);
    const solutions = combinations.filter(c => c.value === target).map(c => printExpression(c.expression));
    return { target, solutions };
  }

}

function generatePuzzle(
  { puzzleNumbersGenerator, combinationsGenerator, targetPicker },
  amountOfBigNumbers, amountOfLittleNumbers
) {
  const generator = new PuzzleGenerator(
    puzzleNumbersGenerator, combinationsGenerator, targetPicker, amountOfBigNumbers, amountOfLittleNumbers
  );
  return generator.generate();
};

module.exports = generatePuzzle;
