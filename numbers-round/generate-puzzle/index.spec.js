'use strict';

const generatePuzzle = require('./');
const { expect } = require('chai');
const vm = require('vm');

describe('Numbers round puzzle generator (integration)', function() {

  const solutionEqualsTarget = function(solution, target) {
    expect(vm.runInNewContext(solution)).to.eql(target);
  };

  it('should be wired correctly', function() {
    this.timeout(3500);
    const puzzle = generatePuzzle(0, 6);
    expect(puzzle.numbers).to.have.lengthOf(6);
    expect(puzzle.numbers.filter(n => n >= 25)).to.have.lengthOf(0);
    expect(puzzle.numbers.filter(n => n <= 10)).to.have.lengthOf(6);
    expect(puzzle.target).to.be.within(101, 999);
    expect(puzzle.solutions).to.have.length.of.at.least(1);
    solutionEqualsTarget(puzzle.solutions[0], puzzle.target);
  });

});
