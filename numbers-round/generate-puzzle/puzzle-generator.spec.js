'use strict';

const generatePuzzle = require('./puzzle-generator');
const chai = require('chai');
const chaiSubset = require('chai-subset');
const { expect } = chai;
const InvalidPuzzleNumbersAmountError = require('./invalid-puzzle-numbers-amount-error');
const genCombs = require('../combinations-generator');

chai.use(chaiSubset);

const throwsInvalidPuzzleNumbersAmountError = function({ amountOfBigNumbers, amountOfLittleNumbers }) {
  try {
    generatePuzzle({}, amountOfBigNumbers, amountOfLittleNumbers);
  } catch (error) {
    expect(error).to.be.instanceOf(InvalidPuzzleNumbersAmountError);
    return;
  }
  throw new Error('Should have thrown error');
};

const doesNotThrowError = function({ amountOfBigNumbers, amountOfLittleNumbers }) {
  const dependencies = {
    puzzleNumbersGenerator: () => {},
    combinationsGenerator: () => [],
    targetPicker: () => 0
  };
  generatePuzzle(dependencies, amountOfBigNumbers, amountOfLittleNumbers);
};

describe('Numbers round puzzle generator', function() {

  describe('should throw InvalidPuzzleNumbersAmountError when given invalid input', function() {

    it('like less than 0 numbers of a kind', function() {
      throwsInvalidPuzzleNumbersAmountError({ amountOfBigNumbers: -1, amountOfLittleNumbers: 0 });
      throwsInvalidPuzzleNumbersAmountError({ amountOfBigNumbers: 0, amountOfLittleNumbers: -1 });
    });

    it('like more than 4 big numbers', function() {
      throwsInvalidPuzzleNumbersAmountError({ amountOfBigNumbers: 5, amountOfLittleNumbers: 0 });
    });

    it('like more than 6 little numbers', function() {
      throwsInvalidPuzzleNumbersAmountError({ amountOfBigNumbers: 0, amountOfLittleNumbers: 7 });
    });

    it('like not 6 numbers altogether', function() {
      throwsInvalidPuzzleNumbersAmountError({ amountOfBigNumbers: 2, amountOfLittleNumbers: 5 });
      throwsInvalidPuzzleNumbersAmountError({ amountOfBigNumbers: 2, amountOfLittleNumbers: 1 });
    });

  });

  it('should not throw error when the given amount of numbers are correct', function() {
    doesNotThrowError({ amountOfBigNumbers: 4, amountOfLittleNumbers: 2 });
    doesNotThrowError({ amountOfBigNumbers: 0, amountOfLittleNumbers: 6 });
  });

  describe('when given correct amount of numbers', function() {

    const bigAmount = 4;
    const littleAmount = 2;

    const fakeGeneratedNumbers = [25, 50, 75, 100, 2, 7];
    const fakePuzzleNumbersGenerator = function(little, big) {
      if (big === bigAmount && little === littleAmount)
        return fakeGeneratedNumbers;
      else
        return [];
    };

    const fakeCombinations = genCombs([3, 5, 7]);
    const fakeCombinationsGenerator = function(numbers) {
      if (numbers === fakeGeneratedNumbers)
        return fakeCombinations;
      else
        return [];
    };

    const fakeTarget = 2;
    const fakeTargetPicker = function(combinations) {
      if (combinations === fakeCombinations)
        return fakeTarget;
      else
        return -1;
    };

    const callGeneratePuzzle = function() {
      return generatePuzzle({
        puzzleNumbersGenerator: fakePuzzleNumbersGenerator,
        combinationsGenerator: fakeCombinationsGenerator,
        targetPicker: fakeTargetPicker
      }, bigAmount, littleAmount);
    };

    it('should generate puzzle numbers', function() {
      const puzzle = callGeneratePuzzle();
      expect(puzzle).to.containSubset({ numbers: fakeGeneratedNumbers });
    });

    it('should pick a target', function() {
      const puzzle = callGeneratePuzzle();
      expect(puzzle).to.containSubset({ target: fakeTarget });
    });

    it('should give the solutions in a human readable format', function() {
      const puzzle = callGeneratePuzzle();
      expect(puzzle).to.containSubset({ solutions: ['7 - 5', '5 - 3'] });
    });

  });


});
