'use strict';

const generatePuzzle = require('./');
const { expect } = require('chai');
const randomWrapper = require('../../util/random-wrapper');

class FakeRandomSequence {

  constructor(sequence) {
    this._sequence = sequence;
    this._nextIndex = 0;
    this.getNext = this.getNext.bind(this);
  }

  getNext() {
    return this._sequence[this._nextIndex++ % this._sequence.length];
  }

}

describe('Numbers round puzzle generator (performance)', function() {

  afterEach(function() {
    randomWrapper.restore();
  });

  it('should generate puzzle in 3.5 seconds for 3 big and 3 little numbers', function() {
    this.timeout(3500);

    const randomSequence = new FakeRandomSequence([
      0.470, 0.396, 0.826, 0.298, 0.171, 0.067, 0.162, 0.145, 0.448, 0.928, 0.098
    ]);
    randomWrapper.stub(randomSequence.getNext);

    const puzzle = generatePuzzle(3, 3);

    expect(puzzle.numbers).to.have.lengthOf(6);
  });

});
