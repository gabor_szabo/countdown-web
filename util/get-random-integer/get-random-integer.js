'use strict';

module.exports = function getRandomInteger(randomFunction, lowerBound, upperBound) {

  return lowerBound + Math.floor(randomFunction() * (upperBound - lowerBound));
};
