'use strict';

const getRandomIntegerWithDeps = require('./get-random-integer');
const _ = require('lodash');

module.exports = _.curry(getRandomIntegerWithDeps)(require('../random-wrapper').random);
