'use strict';

const getRandomInteger = require('./get-random-integer');
const expect = require('chai').expect;

const test = function test(resultOfMathRandom, lowerBound, upperBound, expectedResult) {
  const randomFunction = () => resultOfMathRandom;
  const result = getRandomInteger(randomFunction, lowerBound, upperBound);
  expect(result).to.eql(expectedResult);
};

describe('Get Random Integer', function() {

  it('should use the given function for getting a random number in [0; 1)', function() {
    test(0, 0, 10, 0);
    test(0, 1, 11, 1);
    test(0, 2, 11, 2);
    test(0.1, 0, 10, 1);
    test(0.1, 0, 20, 2);
    test(0.99, 0, 20, 19);
    test(0.6754, 2, 22, 15);
  });

});
