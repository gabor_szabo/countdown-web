'use strict';

module.exports = {

  random() {
    return randomFunction();
  },

  stub(newRandomFunction) {
    randomFunction = newRandomFunction;
  },

  restore() {
    randomFunction = Math.random;
  }

};

let randomFunction = Math.random;
