'use strict';

const expect = require('chai').expect;
const isSublist = require('./');

describe('Is sublist helper', function() {

  it('should return true if the sublist is the empty list', function() {
    expect(isSublist([]).of([])).to.eql(true);
    expect(isSublist([]).of([1])).to.eql(true);
  });

  it('should return false if the sublist is longer than the full list', function() {
    expect(isSublist([1]).of([])).to.eql(false);
    expect(isSublist([1, 2]).of([1])).to.eql(false);
  });

  it('should return false if the sublist contains an item the full list does not', function() {
    expect(isSublist([1]).of([2])).to.eql(false);
    expect(isSublist([1, 2]).of([1, 3])).to.eql(false);
  });

  it('should return true if the full list contains the sub list in different order', function() {
    expect(isSublist([1, 2]).of([2, 1])).to.eql(true);
  });

  it('should return false if the sublist contains more of an item than the full list does', function() {
    expect(isSublist([1, 1]).of([1, 2])).to.eql(false);
  });

  it('integration tests', function() {
    expect(isSublist([3, 1, 5, 1, 3, 8]).of([1, 1, 1, 3, 3, 5, 5, 8, 12])).to.eql(true);
    expect(isSublist([3, 1, 5, 1, 3, 8, 8]).of([1, 1, 1, 3, 3, 5, 5, 8, 12])).to.eql(false);
  });

});
