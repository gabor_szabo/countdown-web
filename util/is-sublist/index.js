'use strict';

const _ = require('lodash');

module.exports = function(subList) {
  return {
    of(fullList) {
      const subListItemCounts = _(subList).groupBy().mapValues(value => value.length).value();
      const fullListItemCounts = _(fullList).groupBy().mapValues(value => value.length).value();
      return _.every(subListItemCounts, (itemCount, item) => fullListItemCounts[item] >= itemCount);
    }
  };
};
