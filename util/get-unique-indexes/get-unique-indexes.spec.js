'use strict';

const getUniqueIndexes = require('./get-unique-indexes');
const expect = require('chai').expect;

class FakeRandomSequence {

  constructor(sequence) {
    this._sequence = sequence;
    this._nextIndex = 0;
    this._withExpectations = false;
    this._expectedLowerBound = null;
    this._expectedUpperBound = null;
    this.getRandomInteger = this.getRandomInteger.bind(this);
  }

  withExpectations(expectedLowerBound, expectedUpperBound) {
    this._withExpectations = true;
    this._expectedLowerBound = expectedLowerBound;
    this._expectedUpperBound = expectedUpperBound;
    return this;
  }

  getRandomInteger(lowerBound, upperBound) {
    if (this._withExpectations) {
      expect(lowerBound).to.eql(this._expectedLowerBound);
      expect(upperBound).to.eql(this._expectedUpperBound);
    }
    return this._sequence[this._nextIndex++];
  }

}

describe('Get Unique Indexes function', function() {

  it('should return empty list for picking zero numbers', function() {
    const result = getUniqueIndexes(() => 0, 1, 0);
    this.expect(result).to.eql([]);
  });

  describe('when asking for one index', function() {

    it('should use the number returned by the random integer function', function() {
      let result = getUniqueIndexes(() => 1, 2, 1);
      this.expect(result).to.eql([1]);
      result = getUniqueIndexes(() => 0, 2, 1);
      this.expect(result).to.eql([0]);
    });

  });

  describe('when asking for two indexes', function() {

    it('should use the numbers returned by the random integer function', function() {
      const randomSequence = new FakeRandomSequence([1, 3]);
      const result = getUniqueIndexes(randomSequence.getRandomInteger, 5, 2);
      this.expect(result).to.eql([1, 3]);
    });

    describe('when the random function returns the same values', function() {

      it('should get random number until a new one is found', function() {
        const randomSequence = new FakeRandomSequence([1, 1, 1, 3]);
        const result = getUniqueIndexes(randomSequence.getRandomInteger, 5, 2);
        this.expect(result).to.eql([1, 3]);
      });

    });

  });

  it('should pass the correct limits to the random integer function', function() {
    const randomSequence = new FakeRandomSequence([2, 2, 4, 4, 2, 0]).withExpectations(0, 5);
    let result = getUniqueIndexes(randomSequence.getRandomInteger, 5, 3);
    this.expect(result).to.eql([2, 4, 0]);
  });


});
