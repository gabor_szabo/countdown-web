'use strict';

const _ = require('lodash');
const getUniqueIndexesWithDeps = require('./get-unique-indexes');

module.exports = _.curry(getUniqueIndexesWithDeps)(require('../get-random-integer'));
