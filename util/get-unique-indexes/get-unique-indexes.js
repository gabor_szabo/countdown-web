'use strict';

function getUniqueIndexes(getRandomInteger, limit, amountToGet) {
  const result = new Set();
  while (amountToGet-- > 0) {
    const nextIndex = getNextUniqueIndex(getRandomInteger, limit, result);
    result.add(nextIndex);
  }
  return [...result];
}

function getNextUniqueIndex(getRandomInteger, limit, occupiedIndexes) {
  let randomInteger = getRandomInteger(0, limit);
  while (occupiedIndexes.has(randomInteger))  {
    randomInteger = getRandomInteger(0, limit);
  }
  return randomInteger;
}


module.exports = getUniqueIndexes;
