'use strict';

let Pool = require('./');
let random = require('../random');

describe('Pool', function() {

  describe('#sample', function() {

    it('should return the one element if there is one possible element', function() {
      let pool = new Pool(['a'], [1]);
      let element = pool.sample();
      this.expect(element).to.eql('a');
    });

    [
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 0.7, expected: 'b' },
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 0.5, expected: 'b' },
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 0.99, expected: 'b' },
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 1, expected: 'b' },
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 0, expected: 'a' },
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 0.25, expected: 'a' },
      { elements: ['a', 'b'], proportions: [1, 1], randomNumber: 0.499, expected: 'a' },
      { elements: ['a', 'b', 'c', 'd'], proportions: [1, 1, 1, 1], randomNumber: 0.6, expected: 'c' }
    ].forEach(function(testCase) {

      it(`should return '${testCase.expected}' for ${testCase.elements} with ${testCase.proportions}
          if the random number is ${testCase.randomNumber}`, function() {

        let pool = new Pool(testCase.elements, testCase.proportions);
        this.sandbox.stub(random, 'take').returns(testCase.randomNumber);
        let element = pool.sample();
        this.expect(element).to.eql(testCase.expected);

      });

    });

  });

});
