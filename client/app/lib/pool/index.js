'use strict';

let random = require('../random');

class Pool {

  constructor(elements, weights) {
    this._elements = elements;
    this._breakpoints = this._aggregateWeightsToBreakpoints(weights);
    this._weightSum = this._last(this._breakpoints);
  }

  _aggregateWeightsToBreakpoints([firstWeight, ...weights]) {
    return weights.reduce((boundaries, curr) => {
      boundaries.push(curr + this._last(boundaries));
      return boundaries;
    }, [firstWeight]);
  }

  _last(array) {
    return array[array.length - 1];
  }

  sample() {
    return this._elements[this._getRandomIndex()];
  }

  _getRandomIndex() {
    let randomNumber = this._weightSum * random.take();
    return this._findFirstBiggerIndex(randomNumber);
  }

  _findFirstBiggerIndex(value) {
    for (let i = 0; i < this._breakpoints.length; i++) {
      if (value < this._breakpoints[i]) return i;
    }
    return this._elements.length - 1;
  }

}

module.exports = Pool;
