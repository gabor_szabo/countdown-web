'use strict';

let random = require('./');

describe('Random module #take', function() {

  it('should return a number from [0; 1)', function() {
    let randomNumber = random.take();
    this.expect(randomNumber).to.be.at.least(0);
    this.expect(randomNumber).to.be.below(1);
  });

});
