'use strict';

let Random = require('random-js');
let randomEngine = Random.engines.browserCrypto || Random.engines.nativeMath;

let randomFunction = function() {
  return Random.real(0, 1, false)(randomEngine);
};

module.exports = { take: randomFunction };
