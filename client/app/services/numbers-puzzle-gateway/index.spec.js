'use strict';

const NumbersPuzzleGateway = require('./');

describe('Numbers puzzle gateway', function() {

  describe('#getPuzzle', function() {

    const NUMBERS_ENDPOINT_URL = '/api/some/endpoint/url';
    const BIG_COUNT = 2;
    const LITTLE_COUNT = 4;
    const EXPECTED_URL = '/api/some/endpoint/url?big=2&little=4';
    const BACKEND_RESPONSE = { someKey: 'some value' };

    let $http;
    let $httpBackend;

    beforeEach(inject(function($injector) {
      $http = $injector.get('$http');
      $httpBackend = $injector.get('$httpBackend');
      $httpBackend.whenGET(EXPECTED_URL).respond(200, BACKEND_RESPONSE);
    }));

    afterEach(function() {
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('gets a puzzle from the backend', function() {
      let puzzle;
      const gateway = new NumbersPuzzleGateway(NUMBERS_ENDPOINT_URL, $http);
      gateway.getPuzzle(BIG_COUNT, LITTLE_COUNT).then(result => puzzle = result);
      $httpBackend.flush();
      this.expect(puzzle).to.eql(BACKEND_RESPONSE);
    });

  });

});
