'use strict';

class NumbersPuzzleGateway {

  constructor(NUMBERS_ENDPOINT_URL, $http) {
    this._endpointUrl = NUMBERS_ENDPOINT_URL;
    this._$http = $http;
  }

  getPuzzle(big, little) {
    return this._$http.get(this._endpointUrl + `?big=${big}&little=${little}`).then(response => response.data);
  }

  static create() {
    return ['NUMBERS_ENDPOINT_URL', '$http', NumbersPuzzleGateway];
  }

}

module.exports = NumbersPuzzleGateway;
