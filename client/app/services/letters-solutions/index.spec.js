'use strict';

let LettersBoardService = require('../letters-board');
const LETTERS_COUNT = 3;
let LettersSolutionsService = require('./');

describe('Letters Solutions Service', function() {

  let lettersBoardService;
  let lettersSolutionsService;
  let $httpBackend;

  let fillBoard = function(letters) {
    letters.forEach(letter => lettersBoardService.add(letter));
  };

  beforeEach(inject(function($injector) {
    let $http = $injector.get('$http');
    $httpBackend = $injector.get('$httpBackend');
    lettersBoardService = new LettersBoardService(LETTERS_COUNT);
    lettersSolutionsService = new LettersSolutionsService(lettersBoardService, $http);
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('@solutions', function() {

    it('should be empty array on init', function() {
      this.expect(lettersSolutionsService.solutions).to.eql([]);
    });

  });

  describe('#fetch', function() {

    it('should fetch the solutions from the API', function() {
      fillBoard(['A', 'B', 'C']);
      $httpBackend.whenGET('/api/letters/solutions/ABC').respond(200, { all: ['a', 'b', 'c'] });
      lettersSolutionsService.fetch();
      $httpBackend.flush();
      this.expect(lettersSolutionsService.solutions).to.eql(['a', 'b', 'c']);
    });

  });

  describe('#clear', function() {

    beforeEach(function() {
      fillBoard(['A', 'B', 'C']);
      $httpBackend.whenGET('/api/letters/solutions/ABC').respond(200, { all: ['a', 'b', 'c'] });
      lettersSolutionsService.fetch();
      $httpBackend.flush();
    });

    it('should clear the @solutions array', function() {
      lettersSolutionsService.clear();
      this.expect(lettersSolutionsService.solutions).to.eql([]);
    });

  });

});
