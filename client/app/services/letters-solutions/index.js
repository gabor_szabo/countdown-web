'use strict';

class LettersSolutionsService {

  constructor(lettersBoard, $http) {
    this._lettersBoard = lettersBoard;
    this._$http = $http;
    this._solutions = [];
  }

  get solutions() {
    return this._solutions;
  }

  fetch() {
    let letters = this._lettersBoard.textContent;
    return this._$http.get(`/api/letters/solutions/${letters}`).then(response => {
      this._solutions = response.data.all;
    });
  }

  clear() {
    this._solutions = [];
  }

  static create() {
    return ['lettersBoard', '$http', LettersSolutionsService];
  }

}

module.exports = LettersSolutionsService;
