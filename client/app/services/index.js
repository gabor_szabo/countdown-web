'use strict';

module.exports = {
  'countdownClock': require('./countdown-clock').create(),
  'lettersBoard': require('./letters-board').create(),
  'lettersRound': require('./letters-round').create(),
  'lettersSolutions': require('./letters-solutions').create(),
  'numbersPuzzleGateway': require('./numbers-puzzle-gateway').create()
};
