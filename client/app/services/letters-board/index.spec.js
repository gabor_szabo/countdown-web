'use strict';

let LettersBoardService = require('./');

describe('Letters Board Service', function() {

  let lettersBoard;
  let numberOfLetters;

  beforeEach(function() {
    numberOfLetters = 3;
    lettersBoard = new LettersBoardService(numberOfLetters);
  });

  describe('on init', function() {

    describe('the board', function() {

      it('should have the given number of items', function() {
        this.expect(lettersBoard.letters).to.have.length(numberOfLetters);
      });

      it('should contain empty letters', function() {
        this.expect(lettersBoard.letters).to.eql([
          { value: '', empty: true, position: 0 },
          { value: '', empty: true, position: 1 },
          { value: '', empty: true, position: 2 }
        ]);
      });

      it('should not be full', function() {
        this.expect(lettersBoard.isFull).to.be.false;
      });

    });

  });

  describe('#add', function() {

    describe('when the board is empty', function() {

      it('should add the letter to the first place', function() {
        lettersBoard.add('A');
        this.expect(lettersBoard.letters).to.eql([
          { value: 'A', empty: false, position: 0 },
          { value: '', empty: true, position: 1 },
          { value: '', empty: true, position: 2 }
        ]);
      });

    });

    describe('when the board is not empty, but has free places', function() {

      beforeEach(function() {
        lettersBoard.add('A');
      });

      it('should add the letter to the first place', function() {
        lettersBoard.add('B');
        this.expect(lettersBoard.letters).to.eql([
          { value: 'A', empty: false, position: 0 },
          { value: 'B', empty: false, position: 1 },
          { value: '', empty: true, position: 2 }
        ]);
      });

    });

    describe('when the board is full', function() {

      beforeEach(function() {
        lettersBoard.add('A');
        lettersBoard.add('B');
        lettersBoard.add('C');
      });

      it('should not add the letter', function() {
        lettersBoard.add('D');
        this.expect(lettersBoard.letters).to.eql([
          { value: 'A', empty: false, position: 0 },
          { value: 'B', empty: false, position: 1 },
          { value: 'C', empty: false, position: 2 }
        ]);
      });

    });

  });

  describe('#remove', function() {

    beforeEach(function() {
      lettersBoard.add('A');
      lettersBoard.add('B');
      lettersBoard.add('C');

      lettersBoard.remove(1);
    });

    it('should remove the letter on the position and shift the letters afterward', function() {
      this.expect(lettersBoard.letters).to.eql([
        { value: 'A', empty: false, position: 0 },
        { value: 'C', empty: false, position: 1 },
        { value: '', empty: true, position: 2 }
      ]);
    });

    it('the next #add operation should add after the last non-removed letter', function() {
      lettersBoard.add('D');
      this.expect(lettersBoard.letters).to.eql([
        { value: 'A', empty: false, position: 0 },
        { value: 'C', empty: false, position: 1 },
        { value: 'D', empty: false, position: 2 }
      ]);
    });

  });

  describe('@isFull', function() {

    it('should be false if the board is not full', function() {
      lettersBoard.add('A');
      this.expect(lettersBoard.isFull).to.be.false;

      lettersBoard.add('B');
      this.expect(lettersBoard.isFull).to.be.false;
    });

    it('should be true if the board is full', function() {
      lettersBoard.add('A');
      lettersBoard.add('A');
      lettersBoard.add('A');
      this.expect(lettersBoard.isFull).to.be.true;
    });

  });

  describe('#clear', function() {

    beforeEach(function() {
      lettersBoard.add('A');
      lettersBoard.add('A');
      lettersBoard.clear();
    });

    it('should remove all letters from board', function() {
      this.expect(lettersBoard.letters).to.eql([
        { value: '', empty: true, position: 0 },
        { value: '', empty: true, position: 1 },
        { value: '', empty: true, position: 2 }
      ]);
    });

    it('the next #add should add to the first position', function() {
      lettersBoard.add('B');
      this.expect(lettersBoard.letters).to.eql([
        { value: 'B', empty: false, position: 0 },
        { value: '', empty: true, position: 1 },
        { value: '', empty: true, position: 2 }
      ]);
    });

  });

  describe('@textContent', function() {

    it('should return the content of the board as plain text', function() {
      lettersBoard.add('A');
      lettersBoard.add('B');
      this.expect(lettersBoard.textContent).to.eql('AB');
    });

  });

});
