'use strict';

class LettersBoardService {

  constructor(boardSize) {
    this._boardSize = boardSize;
    this.clear();
  }

  clear() {
    this._letters = this._createEmptyBoard();
    this._nextInsertPointer = 0;
  }

  _createEmptyBoard() {
    return Array(this._boardSize).fill().map((letter, index) => ({
      position: index,
      value: '',
      empty: true
    }));
  }

  add(letter) {
    if (this.isFull) return;
    this._occupyNextFreePosition(letter);
    this._nextInsertPointer++;
  }

  _occupyNextFreePosition(letter) {
    this._letters[this._nextInsertPointer].value = letter;
    this._letters[this._nextInsertPointer].empty = false;
  }

  remove(position) {
    this._letters.splice(position, 1);
    this._letters.push({ value: '', empty: true });
    this._renumberPositions();
    this._nextInsertPointer--;
  }

  _renumberPositions() {
    this._letters.forEach((letter, index) => letter.position = index);
  }

  get letters() {
    return this._letters;
  }

  get isFull() {
    return this._nextInsertPointer >= this._boardSize;
  }

  get textContent() {
    return this._letters.filter(letter => !letter.empty).map(letter => letter.value).join('');
  }

  static create() {
    return ['LETTER_COUNT', LettersBoardService];
  }

}

module.exports = LettersBoardService;
