'use strict';

let EventEmitter = require('events').EventEmitter;

class FakeAudioElement extends EventEmitter {

  constructor() {
    super();
    this.currentTime = 0;
    this.paused = true;
  }

  addEventListener(event, listener) {
    this.on(event, listener);
  }

  play() {
    this.paused = false;
  }

  pause() {
    this.paused = true;
  }

}

module.exports = FakeAudioElement;
