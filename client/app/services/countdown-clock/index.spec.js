'use strict';

let CountdownClockService = require('./');
let FakeAudioElement = require('./fake-audio.helper');

describe('Countdown Clock service', function() {

  let audioElement;
  let clockService;

  beforeEach(function() {
    audioElement = new FakeAudioElement();
    clockService = new CountdownClockService(audioElement);
  });

  describe('@isPlaying', function() {

    it('should be true if the audio is playing', function() {
      audioElement.play();
      this.expect(clockService.isPlaying).to.be.true;
    });

    it('should be false if the audio is not playing', function() {
      audioElement.pause();
      this.expect(clockService.isPlaying).to.be.false;
    });

  });

  describe('#start', function() {

    beforeEach(function() {
      audioElement.currentTime = 5;
    });

    it('should call the play method on the audio element', function() {
      clockService.start();
      this.expect(audioElement.paused).to.be.false;
    });

    it('should set the currentTime property to zero on the audio element', function() {
      clockService.start();
      this.expect(audioElement.currentTime).to.eql(0);
    });

  });

  describe('#stop', function() {

    beforeEach(function() {
      audioElement.play();
    });

    it('should call the pause method on the audio element', function() {
      clockService.stop();
      this.expect(audioElement.paused).to.be.true;
    });

  });

  describe('!stopped event', function() {

    it('should fire when the audio element is paused', function() {
      let eventFired = false;
      clockService.on('stopped', function() { eventFired = true; });

      audioElement.emit('pause');

      this.expect(eventFired).to.be.true;
    });

  });

});
