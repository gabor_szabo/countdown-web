'use strict';

let EventEmitter = require('events').EventEmitter;

class CountdownClockService extends EventEmitter {

  constructor(audio) {
    super();
    this._audio = audio;
    this._audio.addEventListener('pause', () => this.emit('stopped'));
  }

  get isPlaying() {
    return !this._audio.paused;
  }

  start() {
    this._audio.currentTime = 0;
    this._audio.play();
  }

  stop() {
    this._audio.pause();
  }

  static create() {
    return ['CLOCK_AUDIO', CountdownClockService];
  }

}

module.exports = CountdownClockService;
