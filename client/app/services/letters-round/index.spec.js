'use strict';

let LettersRoundService = require('./');
let LettersBoardService = require('../letters-board');
const LETTERS_COUNT = 3;

describe('Letters Round Service', function() {

  let lettersRoundService;
  let lettersBoardService;
  let lettersSolutionsService;

  beforeEach(function() {
    lettersBoardService = new LettersBoardService(LETTERS_COUNT);
    lettersSolutionsService = { clear: this.sinon.stub() };
    lettersRoundService = new LettersRoundService(lettersBoardService, lettersSolutionsService);
  });

  describe('#addVowel', function() {

    it('should add a vowel to the letters board', function() {
      lettersRoundService.addVowel();
      let letter = lettersBoardService.textContent;
      this.expect(letter).to.have.length(1);
      this.expect('AEOUI').to.contain(letter);
    });

  });

  describe('#addConsonant', function() {

    it('should add a consonant to the letters board', function() {
      lettersRoundService.addConsonant();
      let letter = lettersBoardService.textContent;
      this.expect(letter).to.have.length(1);
      this.expect('BCDFGHJKLMNPQRSTUVWXYZ').to.contain(letter);
    });

  });

  describe('#clearBoard', function() {

    it('should clear the letters board', function() {
      lettersBoardService.add('A');
      lettersBoardService.add('B');
      lettersRoundService.clearBoard();
      this.expect(lettersBoardService.textContent).to.eql('');
    });

    it('should clear solutions', function() {
      lettersRoundService.clearBoard();
      this.expect(lettersSolutionsService.clear).to.be.called;
    });

  });

});
