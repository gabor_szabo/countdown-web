'use strict';

require('babel-polyfill');
let angular = require('angular');

let appModule = angular.module('countdownGame', []);

appModule.directive(require('./directives'));
appModule.service(require('./services'));
appModule.constant('LETTER_COUNT', 9)
         .constant('CLOCK_AUDIO', new Audio('/assets/clock.mp3'))
         .constant('NUMBERS_ENDPOINT_URL', '/api/numbers/puzzle')
         .constant('NUMBERS_SOLUTION_DISPLAY_LIMIT', 3);

angular.element(document).ready(() => {
  angular.bootstrap(document, ['countdownGame']);
});
