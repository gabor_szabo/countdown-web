'use strict';

require('babel-polyfill');
let chai = require('chai');
let sinonChai = require('sinon-chai');

chai.use(sinonChai);

before(function() {
  this.expect = chai.expect;
  this.sinon = sinon;
});

beforeEach(function() {
  this.sandbox = sinon.sandbox.create();
});

afterEach(function() {
  this.sandbox.restore();
});
