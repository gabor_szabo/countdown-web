'use strict';

let LettersSolutionsDirective = require('./');

describe('Letters solutions directive', function() {

  let solutionsService;
  let directive;

  beforeEach(function() {
    solutionsService = {
      solutions: [],
      fetch: this.sinon.stub()
    };
    directive = new LettersSolutionsDirective(solutionsService);
  });

  describe('@solutions', function() {

    it('should be empty array on init', function() {
      this.expect(directive.solutions).to.eql([]);
    });

    it('should reflect the solutions from the service ', function() {
      solutionsService.solutions = ['a', 'b'];
      this.expect(directive.solutions).to.eql(['a', 'b']);
    });

  });

  describe('@error', function() {

    it('should be false on init', function() {
      this.expect(directive.error).to.be.false;
    });

  });

  describe('#findSolutions', function() {

    it('should use the service to fetch the solutions', function() {
      let promise = Promise.resolve(['a', 'b']).then(solutions => solutionsService.solutions = solutions);
      solutionsService.fetch.returns(promise);
      return directive.findSolutions().then(() => {
        this.expect(directive.solutions).to.eql(['a', 'b']);
      });
    });

    it('should set the @error flag to true if there was an error with the solution finding', function() {
      solutionsService.fetch.returns(Promise.reject(new Error('some error')));
      return directive.findSolutions().then(() => {
        this.expect(directive.error).to.be.true;
      });
    });

    it('should set the error flag to false before fetching the solutions', function() {
      solutionsService.fetch.returns(Promise.reject(new Error('some error')));
      return directive.findSolutions().then(() => {
        directive.findSolutions();
        this.expect(directive.error).to.be.false;
      });
    });

  });

});
