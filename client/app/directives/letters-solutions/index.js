'use strict';

class LettersSolutionsDirective {

  constructor(lettersSolutions) {
    this._lettersSolutions = lettersSolutions;
    this._error = false;
  }

  get solutions() {
    return this._lettersSolutions.solutions;
  }

  get error() {
    return this._error;
  }

  findSolutions() {
    this._error = false;
    return this._lettersSolutions.fetch().catch(() => this._error = true);
  }

  static create() {
    return [() => ({
      template: require('./index.jade'),
      scope: {},
      controllerAs: 'ctrl',
      controller: ['lettersSolutions', LettersSolutionsDirective]
    })];
  }

}

module.exports = LettersSolutionsDirective;
