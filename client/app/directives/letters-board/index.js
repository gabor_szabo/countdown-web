'use strict';

class LettersBoard {

  constructor(lettersBoardService) {
    this._lettersBoardService = lettersBoardService;
  }

  get letters() {
    return this._lettersBoardService.letters;
  }

  removeLetter(position) {
    this._lettersBoardService.remove(position);
  }

  static create() {
    return [() => ({
      template: require('./index.jade'),
      scope: {},
      controllerAs: 'ctrl',
      controller: ['lettersBoard', LettersBoard]
    })];
  }

}

module.exports = LettersBoard;
