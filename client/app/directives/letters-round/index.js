'use strict';

class LettersRound {

  constructor(lettersRoundService) {
    this._lettersRoundService = lettersRoundService;
  }

  addConsonant() {
    this._lettersRoundService.addConsonant();
  }

  addVowel() {
    this._lettersRoundService.addVowel();
  }

  clearBoard() {
    this._lettersRoundService.clearBoard();
  }

  static create() {
    return [() => ({
      template: require('./index.jade'),
      scope: {},
      controllerAs: 'ctrl',
      controller: ['lettersRound', LettersRound]
    })];
  }

}

module.exports = LettersRound;
