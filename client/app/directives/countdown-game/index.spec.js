'use strict';

const CountdownGame = require('./');

describe('Countdown Game directive', function() {

  describe('@currentGame', function() {

    it('should be letters on init', function() {
      const countdownGame = new CountdownGame();
      this.expect(countdownGame.currentGame).to.eql('letters');
    });

  });

  describe('#changeGame', function() {

    it('should change @currentGame to the given parameter', function() {
      const countdownGame = new CountdownGame();
      countdownGame.changeGame('otherGameType');
      this.expect(countdownGame.currentGame).to.eql('otherGameType');
    });

  });

});
