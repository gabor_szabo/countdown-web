'use strict';

class CountdownGame {

  constructor() {
    this._currentGame = 'letters';
  }

  get currentGame() {
    return this._currentGame;
  }

  changeGame(newGame) {
    this._currentGame = newGame;
  }

  static create() {
    return [() => ({
      template: require('./index.jade'),
      scope: {},
      controllerAs: 'ctrl',
      controller: [CountdownGame]
    })];
  }

}

module.exports = CountdownGame;
