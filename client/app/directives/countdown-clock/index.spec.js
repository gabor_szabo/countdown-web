'use strict';

let CountdownClockDirective = require('./');
let EventEmitter = require('events').EventEmitter;
let $ = require('jquery');

describe('Countdown clock directive', function() {

  let clockService;

  beforeEach(function() {
    clockService = new EventEmitter();
    clockService.start = this.sinon.stub();
    clockService.stop = this.sinon.stub();
  });

  describe('@isPlaying', function() {

    let directiveCtrl;

    beforeEach(function() {
      directiveCtrl = new CountdownClockDirective(clockService);
    });

    it('should be true if the service is playing', function() {
      clockService.isPlaying = true;
      this.expect(directiveCtrl.isPlaying).to.be.true;
    });

    it('should be false if the service is not playing', function() {
      clockService.isPlaying = false;
      this.expect(directiveCtrl.isPlaying).to.be.false;
    });

  });

  describe('#start', function() {

    it('should call #start on countdown clock service', function() {
      let directiveCtrl = new CountdownClockDirective(clockService);
      directiveCtrl.start();
      this.expect(clockService.start).to.be.called;
    });

  });

  describe('#stop', function() {

    it('should call #stop on countdown clock service', function() {
      let directiveCtrl = new CountdownClockDirective(clockService);
      directiveCtrl.stop();
      this.expect(clockService.stop).to.be.called;
    });

  });

  describe('when audio is stopped', function() {

    beforeEach(function() {
      let module = angular.module('test', []);
      module.directive('countdownClock', CountdownClockDirective.create());
      module.factory('countdownClock', () => clockService);

      angular.mock.module('test');

      clockService.isPlaying = true;
    });

    it('should notify angular about it to make start button visible', inject(function($compile, $rootScope) {
      let markup = '<countdown-clock></countdown-clock>';
      let scope = $rootScope.$new(true);
      let directive = $compile(markup)(scope);
      scope.$digest();

      clockService.isPlaying = false;
      clockService.emit('stopped');

      let html = directive.html();
      let startButtonHidden = $(html).find('.countdown-clock__start-button').hasClass('ng-hide');
      this.expect(startButtonHidden).to.be.false;
    }));

  });

});
