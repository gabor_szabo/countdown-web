'use strict';

class CountdownClockDirective {

  constructor(countdownClock, $scope) {
    this._countdownClock = countdownClock;
    this._countdownClock.on('stopped', () => $scope.$digest());
  }

  get isPlaying() {
    return this._countdownClock.isPlaying;
  }

  start() {
    this._countdownClock.start();
  }

  stop() {
    this._countdownClock.stop();
  }

  static create() {
    return [() => ({
      scope: {},
      template: require('./index.jade'),
      controller: ['countdownClock', '$scope', CountdownClockDirective],
      controllerAs: 'ctrl'
    })];
  }

}

module.exports = CountdownClockDirective;
