'use strict';

const NumbersRoundDirective = require('./');
const STATES = require('./states');

describe('Numbers round directive', function() {

  const numbersPuzzleGateway = {
    getPuzzle: () => Promise.resolve()
  };
  const SOLUTION_DISPLAY_LIMIT = 4;

  let directive;

  beforeEach(function() {
    directive = new NumbersRoundDirective(numbersPuzzleGateway, SOLUTION_DISPLAY_LIMIT);
  });

  describe('@state', function() {

    it('is PUZZLE_CHOOSING on init', function() {
      this.expect(directive.state).to.eql(STATES.PUZZLE_CHOOSING);
    });

  });

  describe('#getPuzzle', function() {

    const BIG_COUNT = 1;
    const LITTLE_COUNT = 5;

    it('sets @state to PUZZLE_FETCHING', function() {
      directive.getPuzzle(BIG_COUNT, LITTLE_COUNT);
      this.expect(directive.state).to.eql(STATES.PUZZLE_FETCHING);
    });

    it('gets the puzzle from the gateway', function() {
      this.sandbox.spy(numbersPuzzleGateway, 'getPuzzle');
      directive.getPuzzle(BIG_COUNT, LITTLE_COUNT);
      this.expect(numbersPuzzleGateway.getPuzzle).to.be.calledWith(BIG_COUNT, LITTLE_COUNT);
    });

    describe('if the operation fails', function() {

      it('sets @state to ERROR', function() {
        this.sandbox.stub(numbersPuzzleGateway, 'getPuzzle').returns(Promise.reject(new Error()));
        return directive.getPuzzle(BIG_COUNT, LITTLE_COUNT).then(() => {
          this.expect(directive.state).to.eql(STATES.ERROR);
        });
      });

    });

    describe('if the operation succeeds', function() {

      const GATEWAY_RESULT = { someKey: 'someValue' };

      beforeEach(function() {
        this.sandbox.stub(numbersPuzzleGateway, 'getPuzzle').returns(Promise.resolve(GATEWAY_RESULT));
      });

      it('sets @puzzle to the value given by the gateway', function() {
        return directive.getPuzzle(BIG_COUNT, LITTLE_COUNT).then(() => {
          this.expect(directive.puzzle).to.eql(GATEWAY_RESULT);
        });
      });

      it('sets @state to DISPLAY_PUZZLE', function() {
        return directive.getPuzzle(BIG_COUNT, LITTLE_COUNT).then(() => {
          this.expect(directive.state).to.eql(STATES.DISPLAY_PUZZLE);
        });
      });

    });

  });

  describe('#showSolution', function() {

    it('sets @state to DISPLAY_SOLUTION', function() {
      directive.showSolution();
      this.expect(directive.state).to.eql(STATES.DISPLAY_SOLUTION);
    });

  });

  describe('#newPuzzle', function() {

    it('sets @state to PUZZLE_CHOOSING', function() {
      directive.showSolution();

      directive.newPuzzle();

      this.expect(directive.state).to.eql(STATES.PUZZLE_CHOOSING);
    });

  });

  describe('@SOLUTION_DISPLAY_LIMIT', function() {

    it('is the value passed in on init', function() {
      this.expect(directive.SOLUTION_DISPLAY_LIMIT).to.eql(SOLUTION_DISPLAY_LIMIT);
    });

  });

});
