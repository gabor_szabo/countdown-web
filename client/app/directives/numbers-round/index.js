'use strict';

const STATES = require('./states');

class NumbersRound {

  constructor(numbersRoundGateway, SOLUTION_DISPLAY_LIMIT) {
    this._state = STATES.PUZZLE_CHOOSING;
    this._gateway = numbersRoundGateway;
    this._puzzle = null;
    this._solution_display_limit = SOLUTION_DISPLAY_LIMIT;
  }

  get state() {
    return this._state;
  }

  get puzzle() {
    return this._puzzle;
  }

  get states() {
    return STATES;
  }

  get SOLUTION_DISPLAY_LIMIT() {
    return this._solution_display_limit;
  }

  newPuzzle() {
    this._state = STATES.PUZZLE_CHOOSING;
  }

  getPuzzle(bigCount, littleCount) {
    this._state = STATES.PUZZLE_FETCHING;
    return this._gateway
      .getPuzzle(bigCount, littleCount)
      .then(puzzle => {
        this._puzzle = puzzle;
        this._state = STATES.DISPLAY_PUZZLE;
      })
      .catch(err => this._state = STATES.ERROR);
  }

  showSolution() {
    this._state = STATES.DISPLAY_SOLUTION;
  }

  static create() {
    return [() => ({
      scope: {},
      template: require('./index.jade'),
      controllerAs: 'ctrl',
      controller: ['numbersPuzzleGateway', 'NUMBERS_SOLUTION_DISPLAY_LIMIT', NumbersRound]
    })];
  }

}

module.exports = NumbersRound;
