'use strict';

const STATES = {
  PUZZLE_CHOOSING: 0,
  PUZZLE_FETCHING: 1,
  DISPLAY_PUZZLE: 2,
  DISPLAY_SOLUTION: 3,
  ERROR: 5
};

module.exports = STATES;
