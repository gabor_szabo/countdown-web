'use strict';

module.exports = {
  'countdownClock': require('./countdown-clock').create(),
  'countdownGame': require('./countdown-game').create(),
  'lettersRound': require('./letters-round').create(),
  'lettersBoard': require('./letters-board').create(),
  'lettersSolutions': require('./letters-solutions').create(),
  'numbersRound': require('./numbers-round').create()
};
